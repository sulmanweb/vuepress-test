module.exports = {
    title: 'Test',
    description: 'Just playing around',
    base: '/vuepress-test/',
    dest: 'public',
    plugins: {
      '@vssue/vuepress-plugin-vssue': {
        // set `platform` rather than `api`
        platform: 'gitlab',
  
        // all other options of Vssue are allowed
        owner: 'sulmanweb',
        repo: 'vuepress-test',
        clientId: '6aa291ce2d55f78c0d3f23433f200b497da2b7f65f1a841464e3435537d39dd3',
        clientSecret: 'b21b73bc43c88e8ebc128df9fd877e44c90c36b549474294840bda107cf3ea5b',
      },
    },
  }